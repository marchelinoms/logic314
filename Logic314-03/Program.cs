﻿using System;

namespace Logic314_03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //tugas();
            //array2DFor();
            //splitAndJoin();
            //substring();
            stringToCharArray();
        }


        static void stringToCharArray() {
            Console.WriteLine("---String to Char Array---");
            Console.Write("Masukkan Kalimat :");
            string kalimat = Console.ReadLine();
            char[] charArray = kalimat.ToCharArray();

            foreach(char ch in charArray) 
            {
                Console.WriteLine(ch);
            }

        }
        static void substring() {
            Console.WriteLine("---Sub String---");
            Console.WriteLine("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring(1,4) : " + kalimat.Substring(1, 4));
            Console.WriteLine("Substring(5,2) :" +kalimat.Substring(5, 2));
            Console.WriteLine("Substring(7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring(9)   : " + kalimat.Substring(9));
        }
        static void splitAndJoin() 
        {
            Console.WriteLine("---Split and Join---");
            Console.WriteLine("Masukkan kalimat :");
            string kalimat = Console.ReadLine();
            string[] katakata = kalimat.Split(" ");

            foreach(string kata in katakata)
            {
                Console.WriteLine(kata);
            }
            
            Console.WriteLine(string.Join(",", katakata));
        }
        static void array2DFor()
        {
            int[,] array = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8 ,9}
            };

            //print using for
            for(int i=0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++) {
                    Console.WriteLine(array[i,j]);
                }
            }
        }
        static void perulanganWhile() {
            Console.WriteLine("---Perulangan While---");
            
            bool ulangi = true;
            int nilai =  1;

            while (ulangi) {
                Console.WriteLine($"Proses ke : {nilai}");
                nilai++;
                Console.Write("ulangi proses? (y/n) : ");
                string input = Console.ReadLine().ToLower();
                if (input == "y")
                {
                    ulangi = true;
                }
                else if (input != "y" && input == "n")
                {
                    ulangi = false;
                }
                else 
                {
                    nilai = 1;
                    Console.WriteLine($"Proses ke : {nilai}");
                    Console.Write("ulangi proses? (y/n) : ");
                    ulangi = true;
                }
            }
        }
        static void perulanganDoWhile() {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            } while (a < 5);
        }
        static void tugas()
        {
            int input;
            menu();
            input = int.Parse(Console.ReadLine());
            switch (input)
            {
                case 1:
                    modOperator();

                    Console.Write("Apakah anda ingin mengulang? (y/n)");
                    string pilihan = Console.ReadLine().ToLower();
                    while (pilihan == "y")
                    {
                        modOperator();
                        Console.Write("Apakah anda ingin mengulang? (y/n) :");
                        pilihan = Console.ReadLine().ToLower();
                    }
                    break;
                case 2:
                    pendapatanPemulung();
                    Console.Write("Apakah anda ingin mengulang? (y/n) :");
                    string pilihan2 = Console.ReadLine().ToLower();
                    while (pilihan2 == "y")
                    {
                        pendapatanPemulung();
                        Console.Write("Apakah anda ingin mengulang? (y/n) :");
                        pilihan2 = Console.ReadLine().ToLower();
                    }
                    break;
                case 3:
                    grade();
                    Console.Write("Apakah anda ingin mengulang? (y/n) :");
                    string pilihan3 = Console.ReadLine().ToLower();
                    while (pilihan3 == "y")
                    {
                        grade();
                        Console.Write("Apakah anda ingin mengulang? (y/n) :");
                        pilihan3 = Console.ReadLine().ToLower();
                    }
                    break;
                case 4:
                    cekAngka();
                    Console.Write("Apakah anda ingin mengulang? (y/n) :");
                    string pilihan4 = Console.ReadLine().ToLower();
                    while (pilihan4 == "y")
                    {
                        cekAngka();
                        Console.Write("Apakah anda ingin mengulang? (y/n) :");
                        pilihan4 = Console.ReadLine().ToLower();
                    }
                    break;
                
                default : 
                    break;
            }
        }
        static void perulanganFor() {

            for (int i = 0; i < 10; i++) {
                Console.WriteLine(i);
            }
            for (int i = 0; i < 10; i++) 
            {
                if (i == 5) 
                {
                    continue;
                }
                Console.WriteLine(i);
            }
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }
        static void menu()
        {
            Console.WriteLine("---Tugas---");
            Console.WriteLine("1. Menghitung Modulo");
            Console.WriteLine("2. Pendapatan Pemulung");
            Console.WriteLine("3. Penentuan Grade Berdasarkan Nilai");
            Console.WriteLine("4. Penentuan Ganjil/Genap");
            Console.WriteLine("0. Exit!");
            Console.Write("Pilihan Anda: ");
        }
        static int modOperator()
        {
            Console.WriteLine("---Modulo Operator---");
            int angka, pembagi, hasil;
            Console.Write("Masukkan angka :");
            angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan pembagi : ");
            pembagi = int.Parse(Console.ReadLine());

            hasil = angka % pembagi;

            if (hasil == 0)
            {
                Console.WriteLine($"{angka} % {pembagi} adalah 0");
            }
            else
            {
                Console.WriteLine($"{angka} % {pembagi} bukan 0 melainkan {hasil}");
            }
            return hasil;
        }
        static int pendapatanPemulung()
        {
            Console.WriteLine("---Pendapatan Merangkai Rokok---");
            int puntungRokok;
            Console.Write("Berapa puntung rokok yang didapat? :");
            puntungRokok = int.Parse(Console.ReadLine());

            int batangRokok = puntungRokok / 8;
            int sisa = puntungRokok % 8;

            if (sisa == 0 && batangRokok > 0)
            {
                Console.WriteLine($"Maka jumlah batang rokok yang dapat dirangkai : {batangRokok}");
            }
            else if (sisa != 0 && batangRokok > 0)
            {
                Console.WriteLine($"Maka jumlah batang rokok yang dapat dirangkai : {batangRokok}");
                Console.WriteLine($"Dengan sisa {sisa} puntung");
            }
            else {
                Console.WriteLine("Input invalid");
            }

            int penghasilan = batangRokok * 500;
            Console.WriteLine($"Maka penghasilan : Rp.{penghasilan}");

            return penghasilan;
        }
        static void grade()
        {
            Console.WriteLine("---Grading Nilai---");

            int input;
            int maxGrade = 100;
            Console.Write("Masukkan Nilai Anda :");
            input = int.Parse(Console.ReadLine());

            if (input >= 80 && input <= maxGrade)
            {
                Console.WriteLine("A");
            }
            else if (input < 80 && input >= 60)
            {
                Console.WriteLine("B");
            }
            else if (input > 0 && input < 60)
            {
                Console.WriteLine("C");
            }
            else
            {
                Console.WriteLine("Nilai tersebut tidak berada dalam range 0-100");
            }
        }
        static void cekAngka()
        {
            Console.WriteLine("---Check Angka---");

            int input, checker;
            Console.Write("Masukkan angka:");
            input = int.Parse(Console.ReadLine());
            checker = input % 2;

            if (checker == 0)
            {
                Console.WriteLine($"angka = {input}");
                Console.WriteLine($"angka {input} adalah genap");
            }
            else
            {
                Console.WriteLine($"angka = {input}");
                Console.WriteLine($"angka {input} adalah ganjil");
            }
        }
    }
}



