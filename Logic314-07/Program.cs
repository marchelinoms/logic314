﻿using System;
using System.Collections.Generic;

namespace Logic314_07
{
    class Mamalia 
    {
        public virtual void pindah() {
            Console.WriteLine("Lari....");
        }
    }
    class Kucing : Mamalia
    { 
    }
    class Paus : Mamalia
    {
        public override void pindah() 
        {
            Console.WriteLine("Berenang...");
        }
    }    
    public class Program
    {
        static List<User> list = new List<User>();
        static void Main(string[] args)
        {
            //listUser();
            //Console.Clear();
            //dateTime();
            //stringDateTime();
            //timeSpan();
            //inheritance();
            Paus paus = new Paus();
            paus.pindah();
        }
        static void overriding() { 
            
        }
        static void inheritance() {
            TypeMobil honda = new TypeMobil();
            honda.Civic();
        }
        static void stringDateTime() {
            Console.WriteLine("---Parsing String into DateTime---");
            Console.Write("Masukkan tanggal (dd/mm/yyyy) :");
            string strTanggal = Console.ReadLine().Trim();

           
           DateTime tanggal = DateTime.Parse(strTanggal);

            Console.Write($"Tanggal: {tanggal.Day}");
            Console.Write($" Bulan: {tanggal.Month}");
            Console.Write($" Tahun: {tanggal.Year}");

        }
        static void timeSpan() { 
        //Menghitung interval waktu

            DateTime date1 = new DateTime(2023,1,1,0,0,0);
            DateTime date2 = DateTime.Now;

            TimeSpan interval = date2 - date1;

            Console.WriteLine($"Total Hari        : {interval.Days}");
            Console.WriteLine($"Total Hari        : {interval.TotalDays}");
            Console.WriteLine($"Perbedaan Waktu   : {interval.Hours}");
            Console.WriteLine($"Total Jam         : {interval.TotalHours}");
            Console.WriteLine($"Perbedaan Menit   : {interval.Minutes}");
            Console.WriteLine($"Total Menit       : {interval.TotalMinutes}");
            Console.WriteLine($"Perbedaan Detik   : {interval.Seconds}");
            Console.WriteLine($"Total Detik       : {interval.TotalSeconds}");
        }
        static void dateTime() {

            //Default Date Time
            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            //Date Time Sekarang
            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);

            // Date Sekarang
            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            //Date Time UTC
            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023,03,09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 03, 09, 11, 45, 00);
            Console.WriteLine(dt6);
        }
        static void listUser()
        {
            bool ulang = true;

            while (ulang)
            {
                
                Console.WriteLine("---Data User---");
                User user = new User();
                Console.Write("Masukkan Nama          :");
                user.Nama = Console.ReadLine();
                Console.Write("Masukkan Umur          :");
                user.Umur = int.Parse(Console.ReadLine());
                Console.Write("Masukkan alamat (kota) :");
                user.Alamat = Console.ReadLine();
                list.Add(user);
                for (int i = 0; i < list.Count; i++)
                {
                    Console.WriteLine($"{i+1}.Nama:{list[i].Nama}\tUmur:{list[i].Umur}\tAlamat :{list[i].Alamat}");
                }
                Console.WriteLine("Tambah data ? (y/n)");
                string input = Console.ReadLine().Trim();
                if (input == "y" || input == "n")
                {
                    if (input == "y")
                    {
                        ulang = true;
                    }
                    else
                    {
                        ulang = false;
                    }
                }
                else
                {
                    Console.WriteLine("Input Invalid");
                }
            }
        }
    }
}







