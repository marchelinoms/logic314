﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic314_07
{
    public class Mobil
    {
        public double kecepatan { get; set; }
        public double bensin { get; set; }
        public double posisi { get; set; }
        public string nama  { get; set; }
        public string platno { get; set; }

        public void utama()
        {
            Console.WriteLine("---Start---");
            Console.WriteLine($"Nama        : {nama}");
            Console.WriteLine($"Plat Nomor  : {platno}");
            Console.WriteLine($"Bensin      : {bensin}");
            Console.WriteLine($"Kecepatan   : {kecepatan}");
            Console.WriteLine($"Posisi      : {posisi}");           
        }
        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }
        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }
        public void isiBensin(double liter)
        {
            this.bensin += liter;
        }
    }
    class TypeMobil : Mobil 
    {
        public void Civic() 
        {
            nama = "Honda Civic";
            platno = "B 1234 ABC";
            bensin = 10;

            utama();
        }
    }

}
