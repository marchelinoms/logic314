CREATE TABLE Penjualan
(
Id INT PRIMARY KEY IDENTITY (1,1),
Nama Varchar(50) NOT NULL,
Harga INT NOT NULL
)

INSERT INTO dbo.Penjualan (Nama,Harga) VALUES
('Indomie',1500),
('Close-Up',3500),
('Pepsodent',3000),
('Brush Formula',2500),
('Roti Manis',1000),
('Gula',3500),
('Sarden',4500),
('Rokok Sampurna',11000),
('Rokok 234',11000)

SELECT Nama,Harga,Harga * 100 as [Harga * 100]
FROM dbo.Penjualan