---------DDL----------
--CREATE DATABASE--
CREATE DATABASE db_Kampus
CREATE DATABASE db_Kampus2
--CREATE TABLE--
CREATE TABLE mahasiswa(
Id bigint PRIMARY KEY IDENTITY(1,1),
[Name] Varchar(50) NOT NULL,
[Address] Varchar(50) NOT NULL,
Email Varchar(255) NULL
)
CREATE TABLE mahasiswa2(
Id bigint PRIMARY KEY IDENTITY(1,1),
[Name] Varchar(50) NOT NULL,
[Address] Varchar(50) NOT NULL,
Email Varchar(255) NULL
)
--Alter--
ALTER TABLE mahasiswa 
ADD [Description] Varchar(255)
---------------------------
ALTER TABLE mahasiswa
DROP COLUMN [Description] 
---------------------------
ALTER TABLE mahasiswa
ALTER COLUMN email Varchar(100)
---------------------------

--Drop--
DROP DATABASE db_Kampus2
------------------------
DROP TABLE mahasiswa2
------------------------
--View--
CREATE VIEW vwMahasiswa
AS
SELECT Id,Name,Address,Email
FROM dbo.mahasiswa
---------------------------
SELECT * 
FROM vwMahasiswa
---------------------------
    ------DML---------
--Insert--
INSERT INTO dbo.mahasiswa(Name,Address,Email) VALUES('Marchel','Medan','marchelinomarsoit@gmail.com')
INSERT INTO dbo.mahasiswa(Name,Address,Email) VALUES
('Toni','Garut','toni@gmail.com'), 
('Isni','Cimahi','isni@gmal.com')
--Select--
SELECT Name,Address,Email 
FROM dbo.mahasiswa
--Update--
UPDATE dbo.mahasiswa 
SET Name = 'Marchelino Marcholippi Sihotang'
WHERE Id = 1
-------------------------------------------------------------------------
