use db_Kampus
CREATE TABLE Biodata 
(
Id bigint PRIMARY KEY IDENTITY (1,1),
DOB datetime NOT NULL,
City Varchar(100)
)
ALTER TABLE dbo.Biodata 
ALTER COLUMN DOB date

INSERT INTO dbo.Biodata (DOB,Mahasiswa_id,City) VALUES
('2000/03/15',1,'Jakarta'),
('1999/09/16',2,'Jakarta'),
('1998/09/15',3,'Jakarta')

SELECT mhs.Id as ID, mhs.Name, bio.City,bio.DOB as TanggalLahir,month(bio.DOB) as BulanLahir
FROM dbo.mahasiswa as mhs
JOIN dbo.Biodata as bio on mhs.Id = bio.Mahasiswa_Id
WHERE mhs.Name ='Toni'

---Order By
SELECT *
FROM dbo.mahasiswa as mhs
JOIN dbo.Biodata as bio on mhs.Id = bio.Mahasiswa_Id
ORDER BY mhs.Id ASC , mhs.Name DESC

--Select Top
SELECT TOP 1 * FROM dbo.mahasiswa
--Between
SELECT * FROM dbo.mahasiswa WHERE dbo.mahasiswa.Id BETWEEN 1 AND 3
SELECT * FROM dbo.mahasiswa WHERE dbo.mahasiswa.Id NOT BETWEEN 1 AND 3 
--Group By
SELECT SUM(id),Name
FROM dbo.mahasiswa
GROUP BY Name

--Having
SELECT Count(id),Name
FROM dbo.mahasiswa
GROUP BY Name
HAVING Count(id) > 1

--Distinct
SELECT DISTINCT Name 
FROM dbo.mahasiswa

--SubString
SELECT SUBSTRING('SQL TUTORIAL',1,2) as Judul

--CharIndex
SELECT CHARINDEX('t','Customer') as Judul

--DataLength
SELECT DATALENGTH('akumau.istirahat') as Judul

--Case
ALTER TABLE dbo.mahasiswa
ADD [Length] smallint

UPDATE dbo.mahasiswa
SET [Length] = 117
WHERE Name LIKE '%Isni%'

SELECT mhs.Id, mhs.Name, mhs.[Address],mhs.[Length],
CASE
	WHEN [Length] > 0 AND [Length] <= 50 THEN  'Short'
	WHEN [Length] > 50 AND [Length] <=100 THEN 'Medium'
	ELSE 'Long'
END AS Ketinggian_Relatif
FROM dbo.mahasiswa as mhs
ORDER BY mhs.[Length] DESC

--Concat
SELECT CONCAT('SQL','IS','FUN!')