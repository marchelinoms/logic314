CREATE DATABASE DBPenerbit

CREATE TABLE tblPengarang 
(
ID INT PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang Varchar(7) NOT NULL,
Nama Varchar (30) NOT NULL,
Alamat Varchar (80) NOT NULL,
Kota Varchar(15) NOT NULL,
Kelamin Varchar(1) NOT NULL
)
INSERT INTO dbo.tblPengarang (Kd_Pengarang,Nama,Alamat,Kota,Kelamin) 
VALUES 
	('P0001','Ashadi','Jl.Beo 25','Yogya','P'),
	('P0002','Rian','Jl.Solo 123','Yogya','P'),
	('P0003','Suwadi','Jl.Semangka 13','Bandung','P'),
	('P0004','Siti','Jl.Durian 15','Solo','W'),
	('P0005','Amir','Jl.Gajah 33','Kudus','P'),
	('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
	('P0007','Jaja','Jl.Singa 7','Bandung','P'),
	('P0008','Saman','Jl.Naga 12','Yogya','P'),
	('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
	('P0010','Fatmawati','Jl.Renjana 4','Bogor','W')

CREATE TABLE tblGaji
(
ID INT PRIMARY KEY IDENTITY (1,1),
Kd_Pengarang Varchar(7) NOT NULL,
Nama Varchar(30) NOT NULL,
Gaji Decimal(18,4) NOT NULL
)
INSERT INTO dbo.tblGaji (Kd_Pengarang,Nama,Gaji) 
VALUES 
	('P0002','Rian',600000),
	('P0005','Amir',700000),
	('P0004','Siti',500000),
	('P0003','Suwadi',1000000),
	('P0010','Fatmawati',600000),
	('P0008','Saman',750000)

--Nomor 1
SELECT COUNT(ID) as Jumlah_Pengarang
FROM tblPengarang
--Nomor 2
SELECT Kelamin,Count(ID)Jumlah
FROM tblPengarang
GROUP BY Kelamin
--Nomor 3
SELECT Kota,Count(Kota)Jumlah
FROM tblPengarang
GROUP BY Kota
--Nomor 4
SELECT Kota
FROM tblPengarang
GROUP BY Kota
HAVING Count(Kota) > 1
--Nomor 5
SELECT MAX(SUBSTRING(Kd_Pengarang,1,5)) as KodeMax, MIN(SUBSTRING(Kd_Pengarang,1,5)) as KodeMin
FROM tblPengarang  
--Nomor 6
SELECT MAX(Gaji) as Gaji_Tertinggi , MIN(Gaji) as Gaji_Terendah
FROM tblGaji
--Nomor 7
SELECT Gaji
FROM tblGaji
WHERE Gaji > 600000
--Nomor 8
SELECT SUM(Gaji) as Total_Gaji
FROM tblGaji
--Nomor 9
SELECT Kota,SUM(gaji.Gaji)as Total
FROM tblGaji as gaji
JOIN tblPengarang as peng on peng.Kd_Pengarang = gaji.Kd_Pengarang
GROUP BY Kota
--Nomor 10
SELECT *
FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0003' AND 'P0006'
--Nomor 11
SELECT *
FROM tblPengarang
WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'
--Nomor 12
SELECT *
FROM tblPengarang
WHERE Kota != 'Yogya'
--Nomor 13
SELECT *
FROM tblPengarang
WHERE Nama LIKE 'A%' OR Nama LIKE '%i' OR Nama LIKE '__a%' OR Nama NOT LIKE '%n'
--Nomor 14
SELECT peng.Kd_Pengarang,peng.Nama,peng.Alamat,peng.Kota,gaji.Gaji
FROM tblGaji as gaji
JOIN tblPengarang as peng on peng.Kd_Pengarang = gaji.Kd_Pengarang
--Nomor 15
SELECT Kota, Gaji
FROM tblGaji as gaji
JOIN tblPengarang as peng on peng.Kd_Pengarang = gaji.Kd_Pengarang
WHERE Gaji < 1000000 AND Gaji > 0
--Nomor 16
ALTER TABLE tblPengarang
ALTER COLUMN Kelamin Varchar(10)
--Nomor 17
ALTER TABLE tblPengarang
ADD Gelar Varchar(12)
--Nomor 18
UPDATE tblPengarang
SET Alamat = 'Jl.Cendrawasih 65', Kota = 'Pekanbaru'
WHERE Nama = 'Rian'
--Nomor 19
--CREATE VIEW vwPengarang
--AS
--SELECT peng.Kd_Pengarang,peng.Nama,peng.Kota,gaji.Gaji
--FROM tblPengarang as peng
--JOIN tblGaji as gaji on peng.Kd_Pengarang = gaji.Kd_Pengarang

SELECT * FROM vwPengarang