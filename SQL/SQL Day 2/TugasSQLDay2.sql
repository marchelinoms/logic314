--Tugas SQL Day 2
CREATE DATABASE DB_Entertainer
GO
USE DB_Entertainer
CREATE TABLE Artis(
Kd_Artis Varchar(100) PRIMARY KEY,
Nm_Artis Varchar(100) NOT NULL,
Jenis_Kelamin Varchar(100) NOT NULL,
Bayaran BigInt,
Award Int,
Negara Varchar(100)
)
INSERT INTO Artis (Kd_Artis,Nm_Artis,Jenis_Kelamin,Bayaran,Award,Negara)
VALUES
('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

CREATE TABLE Film
(
Kd_Film Varchar(10) PRIMARY KEY,
Nm_Film Varchar(55) NOT NULL,
Genre Varchar(55),
Artis Varchar(55),
Produser Varchar(55),
Pendapatan BigInt,
Nominasi Int
)

INSERT INTO Film(Kd_Film,Nm_Film,Genre,Artis,Produser,Pendapatan,Nominasi)
VALUES
('F001','IRON MAN'	,'G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER : CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',6950000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',9230000000,5)

CREATE TABLE Produser
(
Kd_Produser Varchar(50) PRIMARY KEY,
Nm_Produser Varchar(50),
International Varchar(50)
)

INSERT INTO Produser(Kd_Produser,Nm_Produser,International)
VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

CREATE TABLE Negara
(
Kd_Negara Varchar(100) PRIMARY KEY,
Nm_Negara Varchar(100)
)

INSERT INTO Negara(Kd_Negara,Nm_Negara)
VALUES
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

CREATE TABLE Genre 
(
Kd_Genre Varchar(50) PRIMARY KEY,
Nm_Genre Varchar(50)
)

INSERT INTO Genre (Kd_Genre,Nm_Genre)
VALUES
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

--Nomor 1 (Seluruh Pendapatan Marvel)
SELECT Nm_Produser, SUM(Pendapatan)Pendapatan
FROM Film
JOIN Produser as prod on prod.Kd_Produser = Film.Produser
WHERE Nm_Produser = 'MARVEL'
GROUP BY Nm_Produser
--Nomor 2 (Film tanpa Nominasi)
SELECT Nm_Film , Nominasi
FROM Film
WHERE Nominasi = 0
--Nomor 3 (Nama Film dengan huruf depan 'P')
SELECT Nm_Film
FROM Film
WHERE Nm_Film LIKE 'p%'
--Nomor 4 (Nama Film dengan huruf akhir 'Y')
SELECT Nm_Film
FROM Film
WHERE Nm_Film LIKE '%y'
--Nomor 5 (Nama Film mengandung huruf 'D')
SELECT Nm_Film
FROM Film
WHERE Nm_Film LIKE '%d%'
--Nomor 6 (Nama Film dan Artis)
SELECT Nm_Film,Nm_Artis
FROM Film
JOIN Artis on Artis.Kd_Artis = Film.Artis
--Nomor 7 (Nama Film dan Artis dari HK)
SELECT Nm_Film,Negara
FROM Film
JOIN Artis on Film.Artis = Artis.Kd_Artis
WHERE Negara = 'hk'
--Nomor 8 (Nama Film dan Artis dari Negara dengan nama tidak mengandung 'O')
SELECT Nm_Film,Nm_Negara
FROM Film
JOIN Artis on Film.Artis = Artis.Kd_Artis
JOIN Negara on Negara.Kd_Negara = Artis.Negara
WHERE Nm_Negara NOT LIKE '%o%'
--Nomor 9 (Artis yang tidak pernah main film)
-------------------------(CARA 1)
SELECT Nm_Artis
FROM Artis
LEFT JOIN Film on Film.Artis = Artis.Kd_Artis
GROUP BY Nm_Artis
HAVING COUNT(Film.Artis) = 0
-------------------------(CARA 2)
SELECT Nm_Artis
FROM Artis
LEFT JOIN Film on Film.Artis = Artis.Kd_Artis
WHERE Nm_Film IS NULL
--Nomor 10 (Artis dengan genre Drama)
SELECT Nm_Artis,Nm_Genre
FROM Film
JOIN Artis on Film.Artis = Artis.Kd_Artis
JOIN Genre on Genre.Kd_Genre = Film.Genre
WHERE Nm_Genre IN ('DRAMA')
--Nomor 11 (Artis dengan genre Action)
SELECT Nm_Artis,Nm_Genre
FROM Film
JOIN Artis on Film.Artis = Artis.Kd_Artis
JOIN Genre on Genre.Kd_Genre = Film.Genre
WHERE Nm_Genre IN ('ACTION')
GROUP BY Nm_Artis,Nm_Genre
--Nomor 12 (Negara dan Jumlah Film)
SELECT Kd_Negara,Nm_Negara,COUNT(Kd_Film)jumlah_Film
FROM Film
JOIN Artis on Film.Artis = Artis.Kd_Artis
RIGHT JOIN Negara on Negara.Kd_Negara = Artis.Negara 
GROUP BY Kd_Negara,Nm_Negara
ORDER BY Nm_Negara
--Nomor 13 (Film Skala Internasional)
SELECT Nm_Film
FROM Film
JOIN Produser on film.Produser = Produser.Kd_Produser
WHERE International = 'YA'
--Nomor 14 (Film dari masing-masing Produser)
SELECT Nm_Produser,COUNT(Kd_Film)jumlah_film
FROM Film
RIGHT JOIN Produser on film.Produser = Produser.Kd_Produser
GROUP BY Nm_Produser