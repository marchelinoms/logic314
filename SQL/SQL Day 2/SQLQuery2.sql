--SQL Day 2

--Cast
SELECT CAST(10 as decimal(18,4))
SELECT CAST('10' as int)
SELECT CAST('2023-03-16' as datetime)
SELECT GETDATE(),GETUTCDATE()
SELECT MONTH(GETDATE()),DAY(GETDATE()),YEAR(GETDATE())

--Convert
SELECT CONVERT(decimal(18,4),10)
SELECT CONVERT(int,'10')
SELECT CONVERT(date,'2023-03-16')

--Dateadd
SELECT DATEADD(YEAR,2,GETDATE()),DATEADD(MONTH,3,GETDATE()),DATEADD(DAY,5,GETDATE())
--Datediff
SELECT DATEDIFF(DAY,'2023-03-16','2023-03-25'),DATEDIFF(MONTH,'2023-03-16','2024-06-16')
SELECT DATEDIFF(YEAR,'2023-03-16','2030-03-16')
--Subquery
SELECT Name,Address,Length
FROM dbo.mahasiswa
WHERE Length = 
(
SELECT MAX(Length)
FROM dbo.mahasiswa
)
INSERT INTO dbo.mahasiswa
SELECT Name,Address,Email,Length 
FROM mahasiswa
--Create Index
CREATE INDEX index_name ON dbo.Mahasiswa(Name)
CREATE INDEX index_address_email ON dbo.Mahasiswa(Address,Email)
--Kalau mau tidak ada duplicate data, pake kata UNIQUE sebelum INDEX
CREATE UNIQUE INDEX uniqueindex_adrress ON dbo.Mahasiswa(Address)
--Drop Index
DROP INDEX index_address_email ON dbo.Mahasiswa
--Drop Unique Index
DROP INDEX uniqueindex_adress ON dbo.Mahasiswa

--Add Primary Key
ALTER TABLE dbo.Mahasiswa
ADD CONSTRAINT pk_id_address PRIMARY KEY(Id,Address)

--Drop Primary Key
ALTER TABLE dbo.Mahasiswa DROP CONSTRAINT PK__mahasisw__3214EC07CA727759

--NOTE : PRIMARY KEY DALAM SATU TABLE HANYA BISA SATU (SATU CONSTRAINT),
-- BUKAN BERARTI TIDAK BISA LEBIH DARI SATU COLUMN

--Add Unique Constraint
ALTER TABLE dbo.Mahasiswa 
ADD CONSTRAINT unique_Address unique(Address)
