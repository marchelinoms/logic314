--Tugas SQL Day 3 V2---
CREATE DATABASE DB_HR
GO
USE DB_HR
CREATE TABLE tb_karyawan
(
id BIGINT PRIMARY KEY IDENTITY(1,1),
nip VARCHAR(50) NOT NULL,
nama_depan VARCHAR(50) NOT NULL,
nama_belakang VARCHAR(50) NOT NULL,
jenis_kelamin VARCHAR(50) NOT NULL,
agama VARCHAR(50) NOT NULL,
tempat_lahir VARCHAR(50) NOT NULL,
tgl_lahir DATE,
alamat VARCHAR(100) NOT NULL,
pendidikan_terakhir VARCHAR(50) NOT NULL,
tgl_masuk DATE
)

INSERT INTO tb_karyawan (nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
VALUES
('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1997-04-21','Jl.Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl.Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl.Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')

CREATE TABLE tb_divisi
(
id BIGINT PRIMARY KEY IDENTITY(1,1),
kd_divisi VARCHAR(50) NOT NULL,
nama_divisi VARCHAR(50) NOT NULL
)

INSERT INTO tb_divisi (kd_divisi,nama_divisi) 
VALUES ('GD','Gudang'),('HRD','HRD'),('KU','Keuangan'),('UM','Umum')

CREATE TABLE tb_jabatan
(
id BIGINT PRIMARY KEY IDENTITY(1,1),
kd_jabatan VARCHAR(50) NOT NULL,
nama_jabatan VARCHAR(50) NOT NULL,
gaji_pokok NUMERIC,
tunjangan_jabatan NUMERIC
)

INSERT INTO tb_jabatan (kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
VALUES
('MGR','Manager',5500000,1500000),('OB','Office Boy',1900000,200000),('ST','Staff',3000000,750000),('WMGR','Wakil Manager',4000000,1200000)

CREATE TABLE tb_pekerjaan
(
id BIGINT PRIMARY KEY IDENTITY(1,1),
nip VARCHAR(50) NOT NULL,
kode_jabatan VARCHAR(50) NOT NULL,
kode_divisi VARCHAR(50) NOT NULL,
tunjangan_kinerja NUMERIC,
kota_penempatan VARCHAR(50)
)
INSERT INTO tb_pekerjaan(nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan)
VALUES('001','ST','KU',750000,'Cianjur'),('002','OB','UM',350000,'Sukabumi'), ('003','MGR','HRD',1500000,'Sukabumi')


--NO.1(Nama Lengkap,Jabatan,Gaji(Beserta Tunjangan) < 5 Juta)
SELECT CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,nama_jabatan,(gaji_pokok+tunjangan_jabatan)gaji_tunjangan
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_pekerjaan.nip = tb_karyawan.nip
JOIN tb_jabatan ON tb_jabatan.kd_jabatan = tb_pekerjaan.kode_jabatan
WHERE (gaji_pokok+tunjangan_kinerja) < 5000000
ORDER BY gaji_tunjangan DESC

--NO.2(Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi)
SELECT 
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja[total_gaji],
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*0.05 [pajak],
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*0.95 [gaji_bersih]
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_pekerjaan.nip = tb_karyawan.id
JOIN tb_jabatan ON tb_jabatan.kd_jabatan = tb_pekerjaan.kode_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE kota_penempatan NOT IN ('Sukabumi') AND jenis_kelamin = 'PRIA'

--NO.3 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7)
SELECT tb_karyawan.nip,
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,nama_divisi,
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*7/4[Bonus]
FROM tb_karyawan
JOIN tb_pekerjaan on tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan on tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi on tb_divisi.kd_divisi = tb_pekerjaan.kode_divisi
ORDER BY jenis_kelamin

--NO.4 Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR	
SELECT tb_karyawan.nip,
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja[total_gaji],
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*0.05[infak]
FROM tb_karyawan
JOIN tb_pekerjaan on tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan on tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi on tb_divisi.kd_divisi = tb_pekerjaan.kode_divisi
WHERE nama_jabatan = 'MANAGER'

--No.5 Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1									
SELECT 
tb_karyawan.nip,
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,pendidikan_terakhir,
2000000[tunjangan_pendidikan],gaji_pokok+tunjangan_jabatan+2000000[total_gaji]
FROM tb_karyawan
JOIN tb_pekerjaan on tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan on tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE pendidikan_terakhir LIKE 'S1%'
ORDER BY nama_lengkap	

--NO.6 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
SELECT 
tb_karyawan.nip,
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,nama_divisi,
CASE
WHEN nama_jabatan = 'MANAGER' THEN (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*7/4
WHEN nama_jabatan = 'STAFF' THEN (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*5/4
ELSE (gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*1/2
END AS total_gaji
FROM tb_karyawan
JOIN tb_pekerjaan on tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan on tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi on tb_divisi.kd_divisi = tb_pekerjaan.kode_divisi
ORDER BY jenis_kelamin

--No.7 Buatlah kolom nip pada table karyawan sebagai kolom unique	
ALTER TABLE tb_karyawan
ADD CONSTRAINT unique_nip UNIQUE(nip)		

--No.8 Buatlah kolom nip pada table karyawan sebagai index		
CREATE INDEX nip_karyawan ON tb_karyawan(nip)					

--No.9 Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W	
SELECT 
CASE
WHEN nama_belakang LIKE 'w%' THEN CONCAT(nama_depan,' ',UPPER(nama_belakang))
ELSE CONCAT(nama_depan,' ',nama_belakang)
END AS nama_lengkap
FROM tb_karyawan		

--No.10 Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun	
SELECT tb_karyawan.nip,
CONCAT(nama_depan,' ',nama_belakang)nama_lengkap,
nama_jabatan,
nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja[total_gaji],
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*0.1[bonus],
DATEDIFF(YEAR,tgl_masuk,GETDATE())[lama_bekerja]
FROM tb_karyawan
JOIN tb_pekerjaan on tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan on tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi on tb_divisi.kd_divisi = tb_pekerjaan.kode_divisi
WHERE DATEDIFF(YEAR,tgl_masuk,GETDATE()) >= 8																		