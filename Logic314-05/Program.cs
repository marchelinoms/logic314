﻿using System;

namespace Logic314_05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            padLeft();
        }
        static void convertArrayAll() {
            Console.WriteLine("---Convert All Array---");
            Console.Write("Masukkan angka array (pisah pakai koma)");
            int[] ints = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);

            Console.WriteLine(String.Join(" ",ints));
        }
        static void padLeft() {
            Console.WriteLine("---Pad Left---");
            Console.Write("Masukkan Input               : ");
            int input =int.Parse( Console.ReadLine());
            Console.Write("Masukkan Panjang Karakter    :");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Karakter            :");
            char karakter = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Pad Left : {input.ToString().PadLeft(panjang,karakter)}");
        }
        static void contain() {
            Console.WriteLine("--Contain--");
            Console.Write("Masukkan Kalimat :");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan Contain :");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain)) {
                Console.WriteLine($"Kalimat ini ({kalimat}) mengandung kata {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ini ({kalimat}) tidak mengandung kata {contain}");
            }
        }
    }
}
