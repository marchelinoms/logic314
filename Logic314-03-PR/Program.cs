﻿using System;

namespace Logic314_03_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            tugas();
        }
        static void tugas() {
            Console.Clear();
            Console.WriteLine("-----Tugas-----");
            Console.WriteLine("1. Soal 1 : Grade Nilai Mahasiswa");
            Console.WriteLine("2. Soal 2 : Perolehan Poin dari Isi Ulang Pulsa");
            Console.WriteLine("3. Soal 3 : Promo Grabfood");
            Console.WriteLine("4. Soal 4 : MarketPlace Sopi");
            Console.WriteLine("5. Soal 5 : Generasi berdasarkan tanggal Lahir");
            Console.WriteLine("6. Soal 6 : Slip Gaji Pegawai");
            Console.WriteLine("7. Soal 7 : Menghitung BMI");
            Console.WriteLine("8. Soal 8 : Menentukan rata-rata nilai siswa");
            Console.WriteLine("0. Exit ");
            Console.Write("Pilihan Anda : ");

            string menuPilihan = Console.ReadLine();
            switch(menuPilihan)
            {
                case "1":
                    Console.Clear();
                    soalSatu();
                    break;
                case "2":
                    Console.Clear();
                    soalDua();
                    break;
                case "3":
                    Console.Clear();
                    soalTiga();
                    break;
                case "4":
                    Console.Clear();
                    soalEmpat();
                    break;
                case "5":
                    Console.Clear();
                    soalLima();
                    break;
                case "6":
                    Console.Clear();
                    soalEnam();
                    break;
                case "7":
                    Console.Clear();
                    soalTujuh();
                    break;
                case "8":
                    Console.Clear();
                    soalDelapan();
                    break;
                case "0":
                    keluar();
                    break;
                default:
                    Console.WriteLine("Masukkan angka 1-8 untuk memilih menu!");
                    tugas();
                    break;
            }
        }
        static void ulangi() {
            while(true)
            {
                Console.WriteLine("Ulangi soal ini? (y/n) :");
                string ulangi = Console.ReadLine().ToLower();
                if (ulangi == "n")
                { 
                    tugas();
                    break;
                }
                else if (ulangi == "y")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Input Invalid");
                }
            }
        }
        static void keluar()
        {
            while (true)
            {
                Console.WriteLine("Apakah anda yakin akan keluar ? (y/n) : ");
                string keluar = Console.ReadLine().ToLower();
                if (keluar == "y")
                {
                    Console.Clear();
                    Console.WriteLine("Sudah Selesai :)");
                    Environment.Exit(1);
                }
                else if (keluar == "n")
                {
                   
                    tugas();
                }
                else
                {
                    Console.WriteLine("Input invalid!");
                }
            }
        }
        static void soalSatu()
        {
            
            while (true) {
                Console.WriteLine("---Soal 1 ---");
                Console.WriteLine("---Grade Nilai Mahasiswa---");

                int nilai;
                int nilaiMax = 100;

                Console.Write("Masukkan Nilai Mahasiswa:");
                nilai = int.Parse(Console.ReadLine());

                if (nilai >= 90 && nilai <= nilaiMax)
                {
                    Console.WriteLine("Grade A");

                }
                else if (nilai >= 70 && nilai < 90)
                {
                    Console.WriteLine("Grade B");
                }
                else if (nilai >= 50 && nilai < 70)
                {
                    Console.WriteLine("Grade C");
                }
                else if (nilai >= 0 && nilai < 50)
                {
                    Console.WriteLine("E");
                }
                else
                {
                    Console.WriteLine("Nilai tersebut tidak berada dalam range 0-100");
                }
                ulangi();
            }

        }
        static void soalDua()
        {
            while (true) {
                Console.WriteLine("---Soal 2 ---");
                Console.WriteLine("---Perolehan Poin dari Isi Ulang Pulsa---");
                int nominalPulsa;
                int point = 0;
                int nominalMax = 100000;
                Console.Write("Masukkan Nominal Pulsa: ");
                nominalPulsa = int.Parse(Console.ReadLine());

                if (nominalPulsa >= 10000 && nominalPulsa < 25000)
                {
                    point += 80;
                    Console.WriteLine($"Pulsa : {nominalPulsa}");
                    Console.WriteLine($"Point : {point}");
                }
                else if (nominalPulsa >= 25000 && nominalPulsa < 50000)
                {
                    point += 200;
                    Console.WriteLine($"Pulsa : {nominalPulsa}");
                    Console.WriteLine($"Point : {point}");
                }
                else if (nominalPulsa >= 50000 && nominalPulsa < nominalMax)
                {
                    point += 400;
                    Console.WriteLine($"Pulsa : {nominalPulsa}");
                    Console.WriteLine($"Point : {point}");
                }
                else if (nominalPulsa == nominalMax)
                {
                    point += 800;
                    Console.WriteLine($"Pulsa : {nominalPulsa}");
                    Console.WriteLine($"Point : {point}");
                }
                ulangi();
            } 
        }
        static void soalTiga()
        {
            while (true){ 
            Console.WriteLine("---Soal 3---");
            Console.WriteLine("---Promo GrabFood---");
            int belanja, jarak, tambahanOngkir, total=0;
            int ongkosKirim = 5000;
            string kodePromo;
            Console.Write("Input Total Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Jarak Tempuh (km) : ");
            jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Promo : ");
            kodePromo = Console.ReadLine().ToUpper();

            if (belanja >= 30000 && kodePromo == "JKTOVO")
            {
                int diskonMax = 30000;
                double persentaseDiskon = 0.4;
                double diskon = persentaseDiskon * (double)belanja;
                int diskonInt = (int)diskon;
                int hasilDiskon = diskonInt >= diskonMax ? diskonMax : diskonInt;

                    if (jarak < 6 && jarak > 0)
                    {
                        int setelahDiskon = belanja - hasilDiskon;
                        total = setelahDiskon + ongkosKirim;
                    }
                    else if (jarak >= 6)
                    {
                        tambahanOngkir = 1000 * (jarak - 5);
                        ongkosKirim += tambahanOngkir;
                        int setelahDiskon = belanja - hasilDiskon;
                        total = setelahDiskon + ongkosKirim;
                    }
                    else
                    {
                        Console.WriteLine("Input invalid!");
                    }
                    Console.WriteLine($"Belanja       : {belanja}");
                    Console.WriteLine($"Diskon 40%    : {hasilDiskon}");
                    Console.WriteLine($"Ongkir        : {ongkosKirim}");
                    Console.WriteLine($"Total Belanja : {total}");
            }
            else if ((belanja < 30000 && belanja > 0) || (belanja >= 30000 && kodePromo != "JKTOVO"))
            {
                    total = belanja + ongkosKirim;

                    if (jarak < 6 && jarak > 0)
                    {
                    total = belanja + ongkosKirim;
                   
                    }
                    else if (jarak >= 6)
                    {
                    tambahanOngkir = 1000 * (jarak - 5);
                    ongkosKirim += tambahanOngkir;
                    
                    }
                    else
                    {
                    Console.WriteLine("Input Invalid");
                    }

                    Console.WriteLine($"Belanja         : {belanja}");
                    Console.WriteLine($"Ongkir          : {ongkosKirim}");
                    Console.WriteLine($"Total Belanja   : {total}");
                }
                else
            {
                Console.WriteLine("Input Invalid");
            }
                ulangi();
        }
    }

        static void soalEmpat()
        { while (true)
            {
                Console.WriteLine("---Soal Empat---");
                Console.WriteLine("Marketplace Sopi");
                int belanja, ongkosKirim, diskonOngkir = 0, diskonBelanja = 0, total = 0, ongkir,minBelanja = 0;
                string kodeVoucher;

                Console.Write("Input Total Belanja : ");
                belanja = int.Parse(Console.ReadLine());
                Console.Write("Input Ongkos Kirim :");
                ongkosKirim = int.Parse(Console.ReadLine());
                
                while (true) {
                    Console.WriteLine("Voucher Diskon :");
                    Console.WriteLine("1.Min Order 30rb free ongkir 5rb dan potongan harga belanja 5rb");
                    Console.WriteLine("2.Min Order 50rb free ongkir 10rb dan potongan harga belanja 10rb");
                    Console.WriteLine("3.Min Order 1000rb free ongkir 20rb dan potongan harga belanja 10rb");
                    Console.Write("Pilihan Voucher Anda :");
                    kodeVoucher = Console.ReadLine();

                    bool tandaBelanja = belanja > 0;
                    bool tandaOngkos = ongkosKirim > 0;

                    if (tandaBelanja == true && tandaOngkos == true)
                    {
                        switch (kodeVoucher)
                        {
                            case "1":
                                minBelanja = 30000;
                                if (belanja >= minBelanja)
                                {
                                    diskonOngkir = 5000;
                                    diskonBelanja = 5000;
                                }
                                else
                                {
                                    Console.WriteLine("Minimum belanja 30rb untuk menggunakan voucher ini!");
                                    Console.Clear();
                                }
                                break;
                            case "2":
                                minBelanja = 50000;
                                if (belanja >= minBelanja)
                                {
                                    diskonOngkir = 10000;
                                    diskonBelanja = 10000;
                                }
                                else
                                {
                                    Console.WriteLine("Minimum belanja 50rb untuk menggunakan voucher ini!");
                                    Console.Clear();
                                }
                                break;
                            case "3":
                                minBelanja = 100000;
                                if (belanja >= minBelanja)
                                {
                                    diskonOngkir = 20000;
                                    diskonBelanja = 10000;
                                }
                                else
                                {
                                    Console.WriteLine("Minimum belanja 100rb untuk menggunakan voucher ini!");
                                    Console.Clear();
                                }
                                break;
                            default:
                                Console.WriteLine("Anda tidak menggunakan voucher sama sekali.");
                                break;
                        }
                        ongkir = ongkosKirim <= diskonOngkir ? 0 : ongkosKirim - diskonOngkir;
                        total = belanja + ongkir - diskonBelanja;

                        Console.WriteLine("Konfirmasi pilihan (y/n) :");
                        string lanjut = Console.ReadLine().ToLower();
                        
                        if (lanjut == "y")
                        {
                            if (belanja.CompareTo(minBelanja) >= 0)
                            {
                                Console.WriteLine($"Belanja          : {belanja}");
                                Console.WriteLine($"Ongkos Kirim     : {ongkosKirim}");
                                Console.WriteLine($"Diskon Ongkir    : {diskonOngkir}");
                                Console.WriteLine($"Diskon Belanja   : {diskonBelanja}");
                                Console.WriteLine($"Total Pembayaran : {total}");
                                break;
                            }
                        }
                        
                    }
                    else
                    {
                        Console.WriteLine("Input invalid!");
                    }
                }
                ulangi();
            }        
        }
        static void soalLima()
        {
            while (true)
            {
                Console.WriteLine("---Soal Lima---");
                Console.WriteLine("Generasi berdasarkan tanggal Lahir");

                int tahunLahir;
                string nama;
                Console.Write("Masukkan nama anda :");
                nama = Console.ReadLine();
                Console.Write("Tahun berapa anda lahir ? :");
                tahunLahir = int.Parse(Console.ReadLine());

                if (tahunLahir > 1943 && tahunLahir < 1965)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Baby Boomer");
                }
                else if (tahunLahir > 1964 && tahunLahir < 1980)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi X");
                }
                else if (tahunLahir > 1979 && tahunLahir < 1995)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Y");
                }
                else if (tahunLahir > 1994 && tahunLahir < 2016)
                {
                    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Z");
                }
                else
                {
                    Console.WriteLine("Data Generasi anda belum ada, bila ada kesalahan input, mohon untuk...");
                    Console.WriteLine("Input tahun lahir dengan rentang tahun 1944 - 2015");
                }
                ulangi();
            }
            
        }

        static void soalEnam()
        {
            while (true) 
            {
                Console.WriteLine("---Soal Enam---");
                Console.WriteLine("Slip Gaji Pegawai");
                string namaPegawai;
                int gajiPokok, tunjangan, banyakBulan, bruto, netto =0 , bpjs, pajak = 0;

                Console.Write("Nama Pegawai :");
                namaPegawai = Console.ReadLine();
                Console.Write("Gaji Pokok : Rp. ");
                gajiPokok = int.Parse(Console.ReadLine());
                Console.Write("Tunjangan : Rp.");
                tunjangan = int.Parse(Console.ReadLine());
                Console.Write("Banyak Bulan: ");
                banyakBulan = int.Parse(Console.ReadLine());

                //Gaji kotor total
                gajiPokok = gajiPokok <= 0 ? 0 : gajiPokok;
                tunjangan = tunjangan <= 0 ? 0 : tunjangan;
                bruto = banyakBulan * (gajiPokok + tunjangan);
                bruto = bruto <= 0 ? 0 : bruto; 
                
                //BPJS
                bpjs = bruto / banyakBulan * 3 / 100;

                //Gaji bersih
                netto = bruto / banyakBulan - bpjs - pajak;

                if (gajiPokok > 0 && tunjangan > 0)
                {
                    if (bruto / banyakBulan <= 5000000 && bruto / banyakBulan > 0)
                    {
                        pajak = bruto / banyakBulan * 5 / 100;
                    }
                    else if (bruto / banyakBulan > 5000000 && bruto / banyakBulan <= 10000000)
                    {
                        pajak = bruto / banyakBulan * 10 / 100;

                    }
                    else if (bruto / banyakBulan > 10000000)
                    {
                        pajak = bruto / banyakBulan * 15 / 100;
                    }

                    Console.WriteLine($"Karyawan atas nama {namaPegawai} slip gaji sebagai berikut :");
                    Console.WriteLine($"Pajak   (-) :Rp. {pajak}");
                    Console.WriteLine($"BPJS    (-) :Rp. {bpjs}");
                    Console.WriteLine($"Gaji/bln    :Rp. {netto}");
                    Console.WriteLine($"TotalGaji   :Rp. {netto * banyakBulan}");
                }
                else {
                    Console.WriteLine("Input Invalid");
                }
                ulangi();
            }    
        }
        static void soalTujuh()
        { while (true) 
            {
                Console.WriteLine("---Soal Tujuh---");
                Console.WriteLine("Menghitung BMI");

                Console.Write("Masukkan berat anda (kg) :");
                float berat = float.Parse(Console.ReadLine());
                Console.Write("Masukkan tinggi anda (cm) :");
                float tinggi = float.Parse(Console.ReadLine());

                float BMI = (float) berat / (float) Math.Pow((tinggi/100),2);
                Console.WriteLine($"Nilai BMI anda : {BMI}");
                if (BMI > 0 && BMI < 18.5)
                {
                    Console.WriteLine("Anda terlalu kurus");
                }
                else if (BMI >= 18.5 && BMI < 25)
                { 
                    Console.WriteLine("Anda termasuk berbadan langsing");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Anda tergolong gemuk");
                }
                else {
                    Console.WriteLine("Input Invalid");
                }
                ulangi();
            }
            
        }
        static void soalDelapan()
        {
            while (true)
            {
                Console.WriteLine("---Soal Delapan---");
                Console.WriteLine("Menentukan rata-rata nilai siswa");

                Console.Write("Masukkan nilai MTK :");
                int mtk = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai Fisika:");
                int fis = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai Kimia:");
                int kim = int.Parse(Console.ReadLine());
                int total = mtk + fis + kim;
                int ratarata = total / 3;

                if (mtk > 0 && fis > 0 && kim > 0)
                {
                    Console.WriteLine($"Nilai rata-rata : {ratarata}");
                    if (ratarata > 0 && ratarata > 50)
                    {
                        Console.WriteLine("Selamat");
                        Console.WriteLine("Kamu Berhasil");
                        Console.WriteLine("Kamu hebat");
                    }
                    else if (ratarata < 50 && ratarata > 0)
                    {
                        Console.WriteLine("Maaf");
                        Console.WriteLine("Kamu Gagal");
                    }
                }
                else { 
                    Console.WriteLine("Input Invalid");
                }
                ulangi();
            }
           
        }
    }
}
