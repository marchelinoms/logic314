﻿using System;

namespace Logic314_04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //removeString();
            insertString();
        }
        static void removeString() {
            Console.WriteLine("---Remove String---");
            Console.Write("Masukkan Kalimat     : ");
            string nama = Console.ReadLine();
            Console.Write("Isi Parameter Remove : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Remove String ({param}) : {nama.Remove(param)}");
        }

        static void insertString() {
            Console.WriteLine("---Insert String---");
            Console.Write("Masukkan Kalimat     :");
            string nama = Console.ReadLine();
            Console.Write("Isi Parameter Insert :");
            int param = int.Parse(Console.ReadLine()) ;
            Console.Write("Masukkan sisipan :");
            string sisip = Console.ReadLine();

            Console.WriteLine($"Hasil Insert String ({param}) : {nama.Insert(param, sisip)}");
        }
    }
}
