﻿using System;

namespace Logic314_02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ifStatement();
            //elseStatement();
            //nestedIf();
            //ternaryConditional();
            //switchCaseCondiitional();
            //lingkaran();
            //persegi();
            //bangunDatar();

            Console.ReadKey();
        }
        static void ifStatement() {
            Console.WriteLine("---If Statement---");
            int x, y;
            Console.Write("Masukkan nilai x:");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y:");
            y = int.Parse(Console.ReadLine());

            if(x > y)
            {
                Console.WriteLine("x is greater than y");
            }
            if (x < y)
            {
                Console.WriteLine("y is greater than x");
            }
            if (x == y) {
                Console.WriteLine("x is equal to y");            
            }
        }
        static void elseStatement() {
            Console.WriteLine("---Else Statement---");           
            int x, y;
            Console.Write("Masukkan nilai x:");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y:");
            y = int.Parse(Console.ReadLine());
            if (x > y)
            {
                Console.WriteLine("x is greater than y");
            }
            else if (x < y)
            {
                Console.WriteLine("y is greater than x");
            }
            else { 
                Console.WriteLine("x is equal to y");
            }
        }
        static void nestedIf() {
            Console.WriteLine("---Nested If---");
            int nilai;
            int kkm = 75;
            int nilaiMax = 100;
            Console.Write("Masukkan nilai akhirmu :");
            nilai= int.Parse(Console.ReadLine());

            if (nilai >= kkm && nilai < nilaiMax) {
                Console.WriteLine("You've passed the exam!");
                if (nilai == nilaiMax) {
                    Console.WriteLine("Excellent! You got the perfect mark!");
                }
            } else if (nilai > nilaiMax) {
                Console.WriteLine("Maximum Score for this test is 100! , Please input the right score!");
            }
            else
            {
                Console.Write("Sorry, you failed this exam!");
                Console.WriteLine("Please take the make up test!");
            }
        }
        static void ternaryConditional() {
            Console.WriteLine("---Ternary Conditional---");
            int x, y;
            Console.Write("Masukkan nilai x:");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai y:");
            y = int.Parse(Console.ReadLine());

            string z;
            z = x > y ? "x is greater than y" : x < y ? "y is greater than x" : "x is equal to y";
            Console.WriteLine(z);
        }
        static void switchCaseCondiitional() { 
            Console.WriteLine("---Switch Case Conditional ---");
            string input;
            Console.Write("Pick your favourite fruit ( Apple/Banana/Mango) : "  );
            input = Console.ReadLine().ToLower();

            switch (input) {
                case "apple":
                    Console.Write("You got an apple!");
                    break;
                case "banana":
                    Console.Write("You got a banana!");
                    break;
                case "mango":
                    Console.Write("You got a mango!");
                    break;
                default:
                    Console.WriteLine("You got none, since the fruit was not on the list!");
                    break;
            }
        }
        static void lingkaran() {
            Console.WriteLine("---Bangun Lingkaran---");
            const double pi = Math.PI ;
            Console.Write("Masukkan nilai jari-jari(cm):");
            int r = int.Parse(Console.ReadLine());

            double keliling = 2 * pi * r;
            double luas = pi * r * r;

            Console.WriteLine($"Keliling Lingkaran : {keliling} cm");
            Console.WriteLine($"Luas Lingkaran : {luas} cm^2 ");

        }
        static void persegi()
        {
            Console.WriteLine("---Bangun Persegi---");
            Console.Write("Masukkan nilai sisi:");
            int sisi = int.Parse(Console.ReadLine());

            double luas = sisi * sisi;
            double keliling = 4 * sisi;

            Console.WriteLine($"Keliling Persegi : {keliling} cm");
            Console.WriteLine($"Luas Persegi : {luas}  cm^2");
        }
        static void bangunDatar() {
            Console.WriteLine("---Bangun Datar---");
            Console.WriteLine("Pilih salah satu dari bangun datar berikut (Persegi/Lingkaran):");
            string input = Console.ReadLine().ToLower();

            switch (input) {
                case "lingkaran":
                    lingkaran();
                    break;
                case "persegi":
                    persegi();
                    break;
                default:
                    Console.WriteLine("Pilihan kamu belum tersedia");
                    break;  
            }
        }
        static void impFashion()
        {
            Console.WriteLine("---IMP Fashion---");

            Console.Write("Masukkan Kode Baju : ");
            int kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Ukuran: ");
            char kodeUkuran = char.Parse(Console.ReadLine().ToLower());

            if (kodeBaju == 1)
            {
                Console.WriteLine("Merk Baju : IMP ");
                if (kodeUkuran == "s")
                {
                    Console.WriteLine("Harga = 200000");
                }
                else if (kodeUkuran == "m")
                {
                    Console.WriteLine("Harga = 220000");
                }
                else
                {
                    Console.WriteLine("Harga = 250000");
                }
            }
            if (kodeBaju == 2)
            {
                Console.WriteLine("Merk Baju : Prada ");
                {
                    if (kodeUkuran == "s")
                    {
                        Console.WriteLine("Harga = 150000");
                    }
                    else if (kodeUkuran == "m")
                    {
                        Console.WriteLine("Harga = 160000");
                    }
                    else
                    {
                        Console.WriteLine("Harga = 170000");
                    }
                }
            }
            if (kodeBaju == 3)
            {
                Console.WriteLine("Merk Baju : Gucci");
                {
                    if (kodeUkuran == "s")
                    {
                        Console.WriteLine("Harga = 200000");
                    }
                    else if (kodeUkuran == "m")
                    {
                        Console.WriteLine("Harga = 200000");
                    }
                    else
                    {
                        Console.WriteLine("Harga = 200000");
                    }
                }
            }
        }
    }
}
