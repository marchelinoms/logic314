﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization.Formatters;

namespace Logic314_07_Kuis
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soalSatu();
            //soalDua();
            //soalTiga();
            //soalEmpat();
            //soalLima();
            //soalEnam();
            //TanggalUjianFT1();
            soalDuaBelas();
            Console.ReadKey();
        }

        static void soalSatu()
        {
            Console.WriteLine("---Soal 1---");

            //Input
            int x, hasil = 1;
            Console.WriteLine("Faktorial");
            Console.WriteLine("Masukkan nilai x :");
            x = int.Parse(Console.ReadLine());

            //Process
            //List<string> list = new List<string>();
            string temp = "";
            for (int i = x; i >= 1; i--)
            {
                hasil *= i;
                temp  += temp == "" ? i.ToString() : "*" + i.ToString();
                //temp = i.ToString();
                //list.Add(temp);
            }
            Console.Write($"{x}! = ");
            //for (int i = 0; i < list.Count; i++)
            //{
            //    if (i == 0)
            //    {
            //        Console.Write($"{list[list.Count - 1]}X");
            //    }
            //    else
            //    {
            //        if (i > 0 && i < list.Count - 1)
            //            Console.Write($"{list[list.Count - 1 - i]}X");
            //        else
            //            Console.Write($"{list[list.Count - 1 - i]}");
            //    }
            //}

            //Output
            //Console.WriteLine($" = {hasil}");
            Console.WriteLine($"{temp} = {hasil}");
            Console.WriteLine($"Ada {hasil} cara ");

        }
        static void soalDua()
        {
            Console.WriteLine("---Soal 2---");

            //Input
            string input = "";
            Console.WriteLine("SOS Signal");
            Console.WriteLine("Signal Input : ");
            input = Console.ReadLine().ToUpper();


            //Process
            char[] ch = input.ToCharArray();
            int count = 0;
            string tmp = "";
            if (input.Length % 3 != 0)
            {
                Console.WriteLine("Input Invalid!");
            }
            else
            {
                for (int i = 0; i < ch.Length; i += 3)
                {
                    //if (ch[i] == 'S' && ch[i + 1] == 'O' && ch[i + 2] == 'S')
                    //{
                    //}
                    //else
                    //{
                    //    count++;
                    //}
                    if (ch[i] != 'S' || ch[i + 1] != 'O' || ch[i + 2] != 'S')
                    {
                        count++;
                        tmp += "SOS";
                    }
                }
                //Output
                Console.WriteLine($"Total Sinyal salah         : {count}");
                Console.WriteLine($"Total Sinyal benar         : {(ch.Length)/3 - count}");
                Console.WriteLine($"Sinyal yang diterima       : {input}");
                Console.WriteLine($"Sinyal benar               : {tmp}");
            }
        }
        static void soalTiga()
        {
            //Input
            Console.WriteLine("---Soal 3---");
            string inputPinjam = "", inputKembali = "";
            int duration;
            Console.WriteLine("Masukkan Tanggal Peminjaman (dd/mm/yyyy)   : ");
            inputPinjam = Console.ReadLine();
            Console.WriteLine("Durasi Peminjaman (hari)  : ");
            duration = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Tanggal Pengembalian (dd/mm/yyyy) : ");
            inputKembali = Console.ReadLine();

            //Process
            DateTime pinjam = DateTime.Parse(inputPinjam);
            DateTime kembali = DateTime.Parse(inputKembali);
            
            DateTime harusKembali = pinjam.AddDays(duration);
            TimeSpan intervalPengembalian = kembali - harusKembali;
            TimeSpan intervalPinjam = pinjam - kembali;

            if (intervalPinjam.Days > 0)
            {
                Console.WriteLine("Input Salah");
            }
            else
            {
                if (intervalPengembalian.Days > 0)
                {
                    int denda = 500 * intervalPengembalian.Days;
                    //Output 1
                    Console.WriteLine($"Dikarenakan keterlambatan pengembalian buku selama {intervalPengembalian.Days} hari ");
                    Console.WriteLine($"Maka anda harus membayarkan denda sebesar : {denda} ");
                }
                //Output 2
                Console.WriteLine("Terima kasih sudah berkunjung");
            }
        }
        static void soalEmpat()
        {
            //Input
            Console.WriteLine("---Soal 4---");
            string mulai = "";
            int durasi;
            Console.WriteLine("Tanggal Mulai (dd/mm/yyyy) :");
            mulai = Console.ReadLine();
            Console.Write("Durasi Kelas :");
            durasi = int.Parse(Console.ReadLine());
            Console.WriteLine("Hari libur (selain weekend) : ");
            int[] hariLibur = Array.ConvertAll(Console.ReadLine().Split(","),int.Parse);

            //Process
            DateTime tanggalMulai = DateTime.Parse(mulai);
            DateTime FT1 = tanggalMulai;

            //for (int i = 0; i < 10; i++)
            //{
            //    FT1 = tanggalMulai.AddDays(i + 1);
            //    if (DayOfWeek.Sunday == FT1.DayOfWeek || DayOfWeek.Saturday == FT1.DayOfWeek)
            //    {

            //        if (DayOfWeek.Sunday == FT1.DayOfWeek)
            //            FT1 = tanggalMulai.AddDays(1);
            //        if (DayOfWeek.Saturday == FT1.DayOfWeek)
            //            FT1 = tanggalMulai.AddDays(2);
            //    }
            //}

            //Output
            Console.WriteLine($"Kelas akan ujian pada :{FT1}");
 
}
/*
 static void TanggalUjianFT1()
{
    Console.Write("Masukkan tanggal mulai kelas (contoh: 2022-09-16): ");
    DateTime tanggalMulaiKelas = DateTime.Parse(Console.ReadLine()); // input tanggal mulai kelas
    Console.Write("Masukkan durasi kelas dalam hari: ");
    int durasiKelas = int.Parse(Console.ReadLine()); // input durasi kelas dalam hari
    Console.Write("Masukkan jumlah hari libur: ");
    int jumlahHariLibur = int.Parse(Console.ReadLine()); // input jumlah hari libur
    List<DateTime> hariLibur = new List<DateTime>();
    for (int i = 1; i <= jumlahHariLibur; i++) // input tanggal libur
    {
        Console.Write("Masukkan tanggal libur ke-{0} (contoh: 2022-09-26): ", i);
        DateTime tanggal = DateTime.Parse(Console.ReadLine());
        hariLibur.Add(tanggal);
    }
    DateTime tanggalSelesaiKelas = tanggalMulaiKelas.AddDays(durasiKelas - 1); // hitung tanggal selesai kelas
    int jumlahHariKerja = 0; // inisialisasi jumlah hari kerja
    DateTime tanggalSementara = tanggalMulaiKelas; // inisialisasi tanggal sementara
    while (tanggalSementara <= tanggalSelesaiKelas) // hitung jumlah hari kerja
    {
        if (tanggalSementara.DayOfWeek != DayOfWeek.Saturday && tanggalSementara.DayOfWeek != DayOfWeek.Sunday)
        {
            bool hariLiburFlag = false;
            foreach (DateTime tanggalLibur in hariLibur)
            {
                if (tanggalSementara.Date == tanggalLibur.Date)
                {
                    hariLiburFlag = true;
                    break;
                }
            }
            if (!hariLiburFlag)
            {
                jumlahHariKerja++;
            }
        }
        tanggalSementara = tanggalSementara.AddDays(1);
    }
    DateTime tanggalUjian = tanggalSelesaiKelas.AddDays(jumlahHariKerja + 2); // tentukan tanggal pelaksanaan ujian FT1
    while (tanggalUjian.DayOfWeek == DayOfWeek.Saturday || tanggalUjian.DayOfWeek == DayOfWeek.Sunday) // jika tanggal jatuh pada hari Sabtu atau Minggu, tambahkan 1 hari
    {
        tanggalUjian = tanggalUjian.AddDays(1);
    }
    Console.WriteLine("Tanggal pelaksanaan ujian FT1 adalah pada hari {0}, tanggal {1} {2}", tanggalUjian.DayOfWeek, tanggalUjian.Day, tanggalUjian.ToString("MMMM"));
}
*/
        static void soalLima()
        {
            Console.WriteLine("---Soal 5---");
            Console.Write("Masukkan Input : ");
            string input = Console.ReadLine().ToLower();
            string trim = String.Concat(input.Where(a => !Char.IsWhiteSpace(a)));
            int vokal = 0;

            foreach (char s in trim)
            {
                if (s == 'a' || s == 'i' || s == 'u' || s == 'e' || s == 'o')
                {
                    vokal++;
                }
            }

            Console.WriteLine($"Jumlah vokal    :{vokal}");
            Console.WriteLine($"Jumlah konsonan :{trim.Length - vokal}   ");
        }
        static void soalEnam()
        {
            Console.WriteLine("---Soal 6---");
            Console.Write("Masukkan Input :");
            string input = Console.ReadLine().ToLower();
            string trim = String.Concat(input.Where(a => !Char.IsWhiteSpace(a)));

            //Process
            foreach (char s in trim) 
            {     
                  Console.Write("***" + s + "***");
                  Console.WriteLine("\n");
            }
        }            
        static void soalSepuluh() 
        { 
            Console.WriteLine("Masukkan panjang lilin :");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            int max = 0, jumlahMax = 0;

            for(int i = 0; i < lilin.Length; i++) 
            {
                if (lilin[i] > max)
                    max = lilin[i];
                if (lilin[i] == max)
                    jumlahMax++;
            }

            Console.WriteLine($"Nilai tertinggi : {max}");
            Console.WriteLine($"Jumlah nilai tertinggi : {jumlahMax}");
        }
        static void soalSebelas()
        { 
            Console.Write("Masukkan data angka (pisah dengan koma) : ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","),int.Parse);
            Console.Write("Jumlah Rotasi :");
            int rotasi = int.Parse(Console.ReadLine());

            for (int i = 0; i < rotasi; i++) 
            { 
                for(int j = 0; j < arr.Length; j++) 
                { 
                    if(j==0 || j == arr.Length - 1) 
                    {
                        if(j==0) 
                        {
                        rotasi = arr[j];
                        arr[j] = arr[j+1];
                        }
                        else 
                        {
                        arr[arr.Length - 1 ] = rotasi;
                        }
                    }
                    else 
                    {
                    arr[j] = arr[j+1];
                    }
                }
            }
            Console.Write(string.Join(",",arr));
        }
        static void soalDuaBelas()
        { //Input
            Console.Write("Masukkan input (pisah dengan koma)  :");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","),int.Parse);

            //Process
            int tampung = 0; 
              for(int i = 0 ; i < arr.Length ; i++) 
            { 
                for(int j = 0 ; j < arr.Length -1 ; j++) 
                {
                    if (arr[j] > arr[j+1]) 
                    {
                        tampung = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = tampung;
                    }
                }
            }
              //Output
              Console.Write("Hasil Sorting : ");
              Console.Write(String.Join(",",arr));
        }
    }
}
