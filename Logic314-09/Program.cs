﻿using System;
using System.Collections.Generic;

namespace Logic314_09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soalSatu();
            //soalDua();
            //soalTiga();
            //soalTiga_2YangBener();
            //soalTiga_3();
            Console.ReadKey();
        }

        static void soalSatu() 
        {
            Console.WriteLine("---Soal 1---");
            Console.WriteLine("Ojek Online");

            //Input
            Console.WriteLine("Jarak dari Toko ke Customer 1 = 2Km");
            Console.WriteLine("Jarak dari Customer 1 ke Customer 2 = 500m");
            Console.WriteLine("Jarak dari Customer 2 ke Customer 3 = 1,5Km");
            Console.WriteLine("Jarak dari Customer 3 ke Customer 4 = 300m");

            Console.Write("Sampai kemanakah perjalanan kali ini ?  :");
            string input = Console.ReadLine();
            double jarak = 0;
            int bensin = 1;
            string hasil = "";

            //Process
            switch (input) 
            {
                case "1":
                    jarak += 2;
                    hasil += "2Km";
                    break;
                case "2":
                    jarak += 2.5;
                    hasil += "2Km + 500 m";
                    break;
                case "3":
                    jarak += 4;
                    bensin += 1;
                    hasil += "2Km + 500m + 1,5Km";
                    break;
                case "4":
                    jarak  += 4.3;
                    bensin += 1;
                    hasil += "2Km + 500m + 1,5Km + 300m";
                    break;
            }
             
            //Output
            Console.WriteLine($"Jarak Tempuh :{hasil} = {jarak} Km \n Bensin = {bensin} liter");
        }
        static void soalDua() 
        {
            Console.WriteLine("---Soal 2---");
            Console.WriteLine("Menghitung jumlah bahan pembuatan kue");

            //Input
            Console.Write("Berapakah jumlah kue yang akan dibuat? : ");
            int input = int.Parse(Console.ReadLine());
            double terigu = 115;
            double gulaPasir = 190;
            double susu = 100;
            string hasil = "";

            //Process + Output
            hasil += $"Terigu: {Math.Round((terigu*input)/15,2)}gr,\nGula Pasir :{Math.Round((gulaPasir*input)/15,2)}gr,\nSusu: {Math.Round((susu*input)/15,2)} ml";
            Console.WriteLine($"Untuk membuat {input} kue pukis\nDibutuhkan:\n{hasil}");
        }
        static void soalTiga() 
        {
            Console.WriteLine("---Soal 3---");
            Console.WriteLine("Segitiga dengan angka");

            //Input
            Console.Write("Masukkan banyak suku : ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan angka awal : ");
            int angka = int.Parse(Console.ReadLine());
            int rasio = angka;
            int hasil = angka;

            //Process + Output
            for (int y = 1; y <= suku; y++)
            {
                for (int x = 1; x <= suku; x++)
                {                        
                        if (x == 1)
                        {
                            angka = rasio;
                            Console.Write(angka + "\t");
                        }
                        else if (y == suku)
                        {
                            angka += rasio;
                            Console.Write(angka + "\t");
                        }
                        else if (x == y)
                        {
                            hasil += rasio;
                            Console.Write(hasil);
                        }
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }
        }
        static void soalTiga_2YangBener() 
        {

            Console.WriteLine("---Soal 3---");
            Console.WriteLine("Segitiga dengan angka");

            //Input
            Console.Write("Masukkan banyak suku : ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukkan angka awal : ");
            int angka = int.Parse(Console.ReadLine());
            int hasil = angka;

            //Process + Output
            for (int y = 0; y < suku; y++)
            {
                for (int x = 0; x <= y; x++)
                {
                    if (x == 0)
                    {
                        hasil = angka;
                        Console.Write(hasil + "\t");
                    }
                    else if (x == y && x < suku - 1)
                    {
                        hasil += angka * x;
                        Console.Write(hasil);
                    }
                    else if (y == suku - 1)
                    {
                        hasil += angka ;
                        Console.Write(hasil + "\t");
                    }
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }
        }
        static void soalTiga_3() 
        {
            Console.WriteLine("---Soal 3---");
            Console.WriteLine("Segitiga dengan angka");

            //Input
            Console.Write("Masukkan angka awal : ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan banyak suku : ");
            int suku = int.Parse(Console.ReadLine());

            //Process + Output
            for (int i = 0; i < suku; i++)
            {
                int x = 0;
                for (int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j < suku - 1 - i)
                        Console.Write("\t");
                    else
                        Console.Write(x + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
