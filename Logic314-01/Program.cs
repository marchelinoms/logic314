﻿using System;

namespace Logic314_01
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Program program = new Program();
            //program.konversi();

            //operatorAritmatika();

            //operatorModulus();

            //operatorPenugasan();

            //operatorPerbandingan();

            //operatorLogika();

            //penjumlahan();

            Console.ReadKey();  
        }
       
        void konversi() {
            int umur = 19;
            String strUmur = umur.ToString();

            Console.WriteLine(
                "Umur kamu :" + Convert.ToString(umur)
            );
            Console.WriteLine("Umur : " + strUmur);

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = true;

            String myString = Convert.ToString(myInt);
            double myDoubleConv = Convert.ToDouble(myInt);
            int myIntConv = Convert.ToInt32(myDouble);
            String myStringConv = Convert.ToString(myBool);

            Console.WriteLine("Integer to string :" + myString);
            Console.WriteLine("Integer to double :" + myDoubleConv);
            Console.WriteLine("Double to integer :" + myIntConv);
            Console.WriteLine("Boolean to string :" + myStringConv);
        }
        static void operatorAritmatika() {
            int mangga, apel, hasil = 0;
            Console.WriteLine("---Operator Aritmatika---");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;

            Console.WriteLine($"Hasil dari penjumlahan mangga dan apel = {hasil}");
        }
        static void penjumlahan() {
            int mangga, apel;
            Console.WriteLine("---Method Return Type---");
            
            Console.Write("Jumlah mangga :");
            mangga = int.Parse(Console.ReadLine()) ;
            Console.Write("Jumlah apel :");
            apel = int.Parse(Console.ReadLine()) ;

            int hasil = hitungPenjumlahan(mangga, apel) ;
            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }
        static int hitungPenjumlahan(int a, int b) {
            int hasil = 0;
            return hasil = a + b;
        }
        static double operatorModulus() {
            int mangga, orang, hasil = 0;
            Console.WriteLine("---Modulus---");
            Console.Write("Jumlah mangga :");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah orang :");
            orang = int.Parse(Console.ReadLine());

            hasil = mangga % orang;

            Console.WriteLine($"Sisa pembagian mangga adalah = {hasil}");
            return hasil;
        }
        static void operatorPenugasan() {
            int mangga = 10;
            int apel = 8;
            Console.WriteLine("---Operator Penugasan (Assignment Operator)---");

            mangga = 15;
            Console.WriteLine($"mangga = {mangga}");
            Console.WriteLine($"apel = {apel}");

            apel += mangga;
            Console.WriteLine($"apel  += {apel}");

            mangga = 15; apel = 8;
            apel -= mangga;
            Console.WriteLine($"apel  -= {apel}");
            
            mangga = 15; apel = 8;
            apel *= mangga;
            Console.WriteLine($"apel *= {apel}");

            mangga = 15; apel = 8;
            apel /= mangga;
            Console.WriteLine($"apel  /= {apel}");

            mangga = 15; apel = 8;
            apel %= mangga;
            Console.WriteLine($"apel  %= {apel}");
        }
        static void operatorPerbandingan() {
            int mangga, apel = 0;
            Console.WriteLine("---Operator Perbandingan---");

            Console.Write("Jumlah mangga : ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah apel :");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil Perbandingan:");
 
            if (mangga > apel)
            {
                Console.WriteLine($"Mangga > Apel : {mangga > apel}");
                Console.WriteLine($"Mangga != Apel : {mangga != apel}");
                Console.WriteLine("Jumlah mangga ternyata lebih banyak daripada apel");
            }
            else if (mangga < apel)
            {
                Console.WriteLine($"Mangga < Apel : {mangga < apel}");
                Console.WriteLine($"Mangga != Apel : {mangga != apel}");
                Console.WriteLine("Jumlah mangga ternyata lebih sedikit daripada apel");
            }
            else {
                Console.WriteLine($"Mangga == Apel : {mangga == apel}");
                Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
                Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
                Console.WriteLine("Jumlah mangga dan apel ternyata sama");
            }
        }
        static void operatorLogika() {
            int umur = 0; 
            string password;
            Console.WriteLine("---Operator Logika---");

            Console.Write("Masukkan umur anda :");
            umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan password :");
            password= Console.ReadLine();

            //Statement 1
            bool isAdult = umur > 18;
            //Statement 2
            bool isPasswordValid = password == "admin";

            if(isAdult && isPasswordValid ) {
                Console.WriteLine("WELCOME TO THE CLUB");
            } else if (isAdult && !isPasswordValid) {
                Console.WriteLine("Password salah, silahkan coba lagi");                    
            } else if(!isAdult){ 
                Console.WriteLine("Anda belum cukup umur"); 
            }
        }
    }
}
