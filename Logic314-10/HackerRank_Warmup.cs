﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic314_10
{
    internal class HackerRank_Warmup
    {
        static void Main(string[] args)
        {
            //SolveMeFirst();
            //SimpleArraySum();
            //CompareTheTriplets();
            //AVeryBigSum();
            //DiagonalDifference();
            //PlusMinus();
            //Staircase();
            //Mini_MaxSum();
            //BirthDayCakeCandles();
            //TimeConversion();
            //Simulasi01Logic();
            //Simulasi02Logic();
            //Simulasi03Logic();
        }
        static void Simulasi01Logic() 
        {
            //Input
            Console.WriteLine("Masukkan input :");
            string input = Console.ReadLine();
            int count = 0;

            //Process
            foreach (char ch in input) 
            {
                if (char.IsUpper(ch))
                    count++;
            }
            //Output
            Console.WriteLine(count);
        }
        static void Simulasi02Logic()
        {
            Console.WriteLine("Print Invoice");
            Console.WriteLine("Input:");
            Console.Write("Start = ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End = ");
            int end = int.Parse(Console.ReadLine());
            string initial = "XA";
            DateTime date = DateTime.Now;
            //Process + Output
            for (int i = start; i <= end; i++) 
            {
                Console.WriteLine($"{initial}-{date.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5,'0')}");
            }
        }
        static void Simulasi03Logic()
        {
            Console.Write("Isi keranjang 1 : ");
            string input1 = Console.ReadLine();
            input1 = input1 == default ? "0" : input1;
            int count1 = Convert.ToInt32(input1);
            Console.Write("Isi keranjang 2 : ");
            string input2 = Console.ReadLine();
            input2 = input2 == default ? "0" : input2;
            int count2 = Convert.ToInt32(input2);
            Console.Write("Isi keranjang 3 : ");
            string input3 = Console.ReadLine();
            input3 = input3 == default ? "0" : input3;
            int count3 = Convert.ToInt32(input3);
            Console.Write("Keranjang yang diambil :");
            string input4 = Console.ReadLine();
            //Process
            int sisaBuah = 0;
            switch (input4)
            {
                case "1":
                    count1 = 0;
                    break;
                case "2":
                    count2 = 0;
                    break;
                case "3":
                    count3 = 0;
                    break;
            }
            sisaBuah = count1 + count2 + count3;
            //Output
            Console.WriteLine($"Sisa Buah = {sisaBuah}");
        }
        static void Simulasi04Logic() { 
        
        }
        static void SolveMeFirst() {
            Console.Write("Masukkan nilai a:");
            int val1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Masukkan nilai b:");
            int val2 = Convert.ToInt32(Console.ReadLine());
            int sum = SolveMeFirst(val1, val2);
            Console.WriteLine(sum);
        }
        static int SolveMeFirst(int a, int b) 
        {
            return a + b;
        }
        static void SimpleArraySum() 
        {
            Console.Write("Masukkan nilai array :");
            int arCount = Convert.ToInt32(Console.ReadLine().Trim());
            List<int> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt32(arTemp)).ToList();
            int result = SimpleArraySum(ar);
            Console.WriteLine(result);
        }
        static int SimpleArraySum(List<int> ar) 
        {
            int result = 0;
            for (int i = 0; i < ar.Count; i++)
            {
                result += ar[i];
            }
            return result;
        }
        static void CompareTheTriplets() 
        {
            Console.WriteLine("Masukkan nilai Alice :");
            List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

            Console.WriteLine("Masukkan nilai Alice :");
            List<int> b = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(bTemp => Convert.ToInt32(bTemp)).ToList();

            List<int> result = CompareTheTriplets(a, b);
            Console.WriteLine(String.Join(" ", result));
        }
        static List<int> CompareTheTriplets(List<int> a, List<int> b) 
        {
            List<int> result = new List<int>() { 0, 0 };
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] > b[i])
                    result[0]++;
                else if (a[i] < b[i])
                    result[1]++;
            }
            return result;
        }
        static void AVeryBigSum() 
        {
            Console.Write("Berapa banyak elemen ? :");
            int arCount = Convert.ToInt32(Console.ReadLine().Trim());

            Console.Write("Masukkan elemennya(spasi) :");
            List<long> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt64(arTemp)).ToList();

            long result = AVeryBigSum(ar);
            Console.WriteLine(result);
        }
        static long AVeryBigSum(List<long> ar) 
        {
            long result = 0;
            for (int i = 0; i < ar.Count; i++)
            {
                result += ar[i];
            }
            return result;
        }
        static void DiagonalDifference()
        {
            Console.WriteLine("Berapa baris ? : ");
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            Console.WriteLine("Masukkan isi matriks : ");
            List<List<int>> arr = new List<List<int>>();
            for (int i = 0; i < n; i++)
            {
                arr.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList());
            }
            int result = DiagonalDifference(arr);
            Console.WriteLine(result);
        }
        static int DiagonalDifference(List<List<int>> arr) 
        {
            int result, a = 0, b = 0;
            for (int i = 0; i < arr.Count; i++)
            {
                a += arr[i][i];
                b += arr[i][arr.Count - 1 - i];
            }
            result = Math.Abs(a - b);
            return result;
        }
        static void PlusMinus() 
        {
            Console.Write("Berapa banyak elemen ? :");
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            Console.Write("Masukkan isi array (spasi) :");
            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            PlusMinus(arr);
        }
        static void PlusMinus(List<int> arr) 
        {
            float pos = 0, neg = 0, zero = 0;
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] > 0)
                    pos++;
                else if (arr[i] < 0)
                    neg++;
                else
                    zero++;
            }
            Console.WriteLine(pos /= arr.Count);
            Console.WriteLine(neg /= arr.Count);
            Console.WriteLine(zero /= arr.Count);
        }
        static void Staircase() 
        {
            Console.Write("Masukkan Input : ");
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            Staircase(n);
        }
        static void Staircase(int n)
        {

            for (int y = n - 1; y >= 0; y--)
            {
                for (int x = 0; x < n; x++)
                {
                    if (x >= y)
                        Console.Write("#");
                    else
                        Console.Write(" ");
                }
                Console.WriteLine("");
            }
        }
        static void Mini_MaxSum() 
        {
            Console.WriteLine("Masukkan Input :");
            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            Mini_MaxSum(arr);
        }
        static void Mini_MaxSum(List<int> arr) 
        {
            long total1 = 0, total2 = 0;
            List<long> array = new List<long>();
            //Sort by Ascending
            for (int i = 0; i < arr.Count; i++)
            {
                int temp = 0;
                for (int j = 0; j < arr.Count - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            //Input the Sorted list to a new List
            for (int i = 0; i < arr.Count; i++)
            {
                long temp = 0;
                for (int j = 0; j < arr.Count; j++)
                {
                    if (i != j)
                    {
                        temp += Convert.ToInt64(arr[j]);
                    }
                }
                array.Add(temp);
            }
            //Counting Min/Max
            for (int i = 0; i < array.Count - 1; i++)
            {
                total1 += arr[i];
            }
            for (int i = array.Count - 1; i > 0; i--)
            {
                total2 += arr[i];
            }
            string result = total1 < total2 ? $"{total1} {total2}" : $"{total2} {total1}";
            Console.Write(result);
        }
        static void BirthDayCakeCandles() 
        {
            Console.Write("Masukkan banyak lilin :");
            int candlesCount = Convert.ToInt32(Console.ReadLine().Trim());
            Console.Write("Masukkan tinggi lilin (spasi) :");
            List<int> candles = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(candlesTemp => Convert.ToInt32(candlesTemp)).ToList();

            int result = BirthDayCakeCandles(candles);
            Console.WriteLine(result);

        }
        static int BirthDayCakeCandles(List<int>candles) 
        {
            int max = candles.Max();
            int count = 0;
            for (int i = 0; i < candles.Count; i++)
            {
                if (candles[i] == max)
                    count++;
            }
            return count;
        }
        static void TimeConversion()
        {
            Console.Write("Masukkan waktu (hh:mm:ss) AM/PM :");
            string s = Console.ReadLine();

            string result = TimeConversion(s);
            Console.WriteLine(result);
        }
        static string TimeConversion(string s) 
        {
            string format = s.Substring(8, 2).ToUpper();
            int jam = Convert.ToInt16(s.Substring(0, 2));
            string result = "";

            if (format == "PM" && jam <= 12)
                result = jam == 12 ? s.Substring(0, 8) : $"{jam + 12}{s.Substring(2, 6)}";
            else if (format == "AM" && jam <= 12)
                result = jam == 12 ? $"00{s.Substring(2, 6)}" : s.Substring(0, 8);
            return result;
        }
    }
}
