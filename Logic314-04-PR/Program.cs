﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;

namespace Logic314_04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soalSatu();
            //soalDua();
            //soalTigaEmpatLima();
            //soalEnamTujuh();
            //soalDelapan();
            //soalSembilan();
            //soalSepuluh();
            //soalSebelas();
            //soalDuaBelas();
            //tugas();
            //soalDelapan2();
            soalDuaBelas2();
        }
        static void tugas()
        {
            Console.Clear();
            Console.WriteLine("-----Tugas-----");
            Console.WriteLine("Soal 1 : Perhitungan Gaji Karyawan......      (1)");
            Console.WriteLine("Soal 2 : Word Counting / Split String.....    (2)");
            Console.WriteLine("Soal 3,4,5 : Another String Manipulation..... (3)");
            Console.WriteLine("Soal 6,7 : Deret.....                         (4)");
            Console.WriteLine("Soal 8 : Fibonacci Sequence.....              (5)");
            Console.WriteLine("Soal 9 : Time Sequence....                    (6)");
            Console.WriteLine("Soal 10 : IMP Fashion.....                    (7)");
            Console.WriteLine("Soal 11 : Belanja Lebaran.....                (8)");
            Console.WriteLine("Soal 12 : 2D Array.....                       (9)");
            Console.WriteLine("0. Exit ");
            Console.Write("Pilihan Anda : ");

            string menuPilihan = Console.ReadLine();
            switch (menuPilihan)
            {
                case "1":
                    Console.Clear();
                    soalSatu();
                    break;
                case "2":
                    Console.Clear();
                    soalDua();
                    break;
                case "3":
                    Console.Clear();
                    soalTigaEmpatLima();
                    break;
                case "4":
                    Console.Clear();
                    soalEnamTujuh();
                    break;
                case "5":
                    Console.Clear();
                    soalDelapan();
                    break;
                case "6":
                    Console.Clear();
                    soalSembilan();
                    break;
                case "7":
                    Console.Clear();
                    soalSepuluh();
                    break;
                case "8":
                    Console.Clear();
                    soalSebelas();
                    break;
                case "9":
                    Console.Clear();
                    soalDuaBelas();
                    break;
                case "0":
                    keluar();
                    break;
                default:
                    Console.WriteLine("Masukkan angka 1-9 untuk memilih menu!");
                    tugas();
                    break;
            }
        }
        static void ulangi()
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Ulangi soal ini? (y/n) :");
                string ulangi = Console.ReadLine().ToLower();
                if (ulangi == "n")
                {
                    tugas();
                    break;
                }
                else if (ulangi == "y")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Input Invalid");
                }
            }
        }
        static void keluar()
        {
            while (true)
            {
                Console.WriteLine("Apakah anda yakin akan keluar ? (y/n) : ");
                string keluar = Console.ReadLine().ToLower();
                if (keluar == "y")
                {
                    Console.Clear();
                    Console.WriteLine("Sudah Selesai :)");
                    Environment.Exit(1);
                }
                else if (keluar == "n")
                {

                    tugas();
                }
                else
                {
                    Console.WriteLine("Input invalid!");
                }
            }
        }
        static void soalSatu()
        {
            while (true)
            {
                Console.WriteLine("---Soal 1---");
                Console.WriteLine("Penghitungan Gaji Karyawan");
                int jamKerja;
                float upah = 0, upahLembur = 0, faktorPengali = 1.5f;

                Console.Write("Golongan  :");
                string golongan = Console.ReadLine();
                Console.Write("Jam Kerja :");
                jamKerja = int.Parse(Console.ReadLine());


                if (jamKerja > 0)
                {
                    switch (golongan)
                    {
                        case "1":

                            upah = jamKerja <= 40 ? 2000 * jamKerja : 40 * 2000;
                            upahLembur = 2000 * faktorPengali * (jamKerja - 40);

                            break;
                        case "2":

                            upah = jamKerja <= 40 ? 3000 * jamKerja : 40 * 3000;
                            upahLembur = 3000 * faktorPengali * (jamKerja - 40);

                            break;
                        case "3":

                            upah = jamKerja <= 40 ? 4000 * jamKerja : 40 * 4000;
                            upahLembur = 4000 * faktorPengali * (jamKerja - 40);

                            break;
                        case "4":

                            upah = jamKerja <= 40 ? 5000 * jamKerja : 40 * 5000;
                            upahLembur = 5000 * faktorPengali * (jamKerja - 40);

                            break;
                        default:
                            Console.WriteLine("Input Invalid");
                            break;
                    }

                    Console.WriteLine($"Upah    : {upah}");
                    Console.WriteLine($"Lembur  : {upahLembur}");
                    Console.WriteLine($"Total   : {upah + upahLembur}");
                }
                else
                {
                    Console.WriteLine("Input Invalid");
                }
                ulangi();
            }
        }
        static void soalDua()
        {
            while (true)
            {
                Console.WriteLine("---Soal 2---");
                Console.WriteLine("Word Counting/Split String");

                Console.WriteLine("Masukkan kalimat :");
                string kalimat = Console.ReadLine();
                string[] katakata = kalimat.Split(" ");

                for (int i = 0; i < katakata.Length; i++)
                {
                    Console.Write("kata " + (i + 1) + " = ");
                    Console.WriteLine(katakata[i]);
                }
                Console.WriteLine("Total kata adalah : " + katakata.Length);
                ulangi();
            }
        }
        static void soalTigaEmpatLima()
        {
            while (true)
            {
                Console.WriteLine("---Soal 3,4,5---");
                Console.WriteLine("Another String Manipulation");

                Console.WriteLine("Masukkan kalimat :");
                string kalimat = Console.ReadLine();
                string[] katakata = kalimat.Split(' ');

                Console.WriteLine("\t Pilih Manipulasi String:");
                Console.WriteLine("1.Sensor karakter di tengah tiap kata");
                Console.WriteLine("2.Sensor karakter di ujung kata");
                Console.WriteLine("3.Sensor karakter di awal kata");
                Console.WriteLine("Pilihan anda :");
                string pilih = Console.ReadLine().Trim();

                switch (pilih)
                {
                    case "1":
                        foreach (String s in katakata)
                        {
                            char[] chars = s.ToCharArray();
                            for (int i = 0; i < chars.Length; i++)
                            {
                                if (i == 0 || i == chars.Length - 1)
                                {
                                    if (i == chars.Length - 1)
                                    {
                                        Console.Write(chars[i] + " ");
                                    }
                                    else
                                    {
                                        Console.Write(chars[i]);
                                    }
                                }
                                else
                                {
                                    Console.Write("*");
                                }
                            }

                        }
                        break;

                    case "2":
                        foreach (String s in katakata)
                        {
                            char[] chars = s.ToCharArray();

                            for (int i = 0; i < chars.Length; i++)
                            {
                                if (i == 0 || i == chars.Length - 1)
                                {
                                    if (i == chars.Length - 1)
                                    {

                                        Console.Write("* ");
                                    }
                                    else
                                    {
                                        Console.Write("*");
                                    }
                                }
                                else
                                {
                                    Console.Write(chars[i]);
                                }
                            }
                        }
                        break;
                    case "3":
                        foreach (String s in katakata)
                        {
                            char[] chars = s.ToCharArray();

                            for (int i = 0; i < chars.Length; i++)
                            {
                                if (i == 0 || i == chars.Length - 1)
                                {
                                    if (i == chars.Length - 1)
                                    {

                                        Console.Write(chars[i] + " ");
                                    }
                                    else
                                    {
                                        Console.Write("");
                                    }
                                }
                                else
                                {
                                    Console.Write(chars[i]);
                                }
                            }
                        }
                        break;
                    default:
                        Console.WriteLine("Input Invalid");
                        break;
                }
                ulangi();
            }

        }
        static void soalEnamTujuh()
        {
            while (true)
            {
                Console.WriteLine("---Soal 6, 7---");
                Console.WriteLine("Deret (2)");

                Console.Write("Masukkan angka : ");
                int angka = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai beda/rasio : ");
                int rasio = int.Parse(Console.ReadLine());
                Console.Write("Sampai suku ke - ? : ");
                int n = int.Parse(Console.ReadLine());
                Console.WriteLine("1.Deret *");
                Console.WriteLine("2.Deret -");
                Console.WriteLine("Pilihan anda :");
                string pilih = Console.ReadLine();

                switch (pilih)
                {
                    case "1":
                        Console.Write(angka + " ");
                        for (int i = 0; i < n - 1; i++)
                        {
                            angka *= rasio;
                            if (i % 2 == 0)
                            {
                                Console.Write(angka + " ");
                            }
                            else
                            {
                                Console.Write(angka + " ");
                            }
                        }
                        break;
                    case "2":
                        for (int i = 0; i < n; i++)
                        {
                            Console.Write(angka + " ");
                            if (angka < 0)
                            {
                                angka += (rasio * -1);
                                angka = Math.Abs(angka);

                            }
                            else
                            {
                                angka += rasio;
                                angka *= -1;
                            }
                        }
                        break;
                    default:
                        Console.WriteLine("Input Invalid");
                        break;
                }
                ulangi();
            }
        }
        static void soalDelapan()
        {
            while (true)
            {
                Console.WriteLine("---Soal 8---");
                Console.WriteLine("Fibonacci Sequence");
                int a = 0, b = 1, c;

                Console.Write("Masukkan nilai n : ");
                int n = int.Parse(Console.ReadLine());

                Console.Write(b + " ");
                for (int i = 1; i < n; ++i)
                {
                    c = a + b;
                    Console.Write(c + " ");
                    a = b;
                    b = c;
                }
                ulangi();
            }
        }
        static void soalDelapan2()
        {
            Console.WriteLine("---Soal 8---");
            Console.WriteLine("Fibonacci Sequence");


            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            int[] nArray = new int[n];
            for (int i = 0; i < n; ++i)
            {
                if (i <= 1)
                {
                    nArray[i] = 1;
                }
                else
                {
                    nArray[i] = nArray[i - 1] + nArray[i - 2];
                }
            }
            Console.Write(string.Join(" ", nArray));
        }
        static void soalSembilan()
        {
            while (true)
            {
                Console.WriteLine("---Soal 9---");
                Console.WriteLine("Time Conversion");

                Console.Write("Masukkan Waktu dalam format 12 jam (hh:mm:ss) AM/PM :");
                string input = Console.ReadLine().Trim();

                string format = input.Substring(8, 2).ToUpper();
                int jam = Convert.ToInt16(input.Substring(0, 2));

                string result = "";
                if (format == "PM")
                {
                    if (jam <= 12)
                    {
                        jam += 12;
                        result = jam.ToString() + input.Substring(2, 6);
                    }
                    else
                    {
                        result = "format 12 jam, masukkan input antara 1-12 AM/PM ";
                    }
                }
                Console.WriteLine("");
                Console.WriteLine(result);
                ulangi();
            }
        }
        static void soalSepuluh()
        {
            while (true)
            {

                Console.WriteLine("---Soal 10---");
                Console.WriteLine("IMP Fashion");
                string baju, ukuran, merk = "";
                int harga = 0;
                Console.Write("Masukkan Kode Baju : ");
                baju = Console.ReadLine();
                Console.Write("Masukkan Kode Ukuran : ");
                ukuran = Console.ReadLine().ToLower();

                if (ukuran.GetType() != typeof(string))
                {
                    Console.WriteLine("Input Invalid");
                }

                switch (baju)
                {
                    case "1":
                        merk = "IMP";
                        if (ukuran == "s")
                        {
                            harga = 200000;
                        }
                        else if (ukuran == "m")
                        {
                            harga = 220000;
                        }
                        else
                        {
                            harga = 250000;
                        }
                        break;

                    case "2":
                        merk = "Prada";
                        if (ukuran == "s")
                        {
                            harga = 15000;
                        }
                        else if (ukuran == "m")
                        {
                            harga = 160000;
                        }
                        else
                        {
                            harga = 170000;
                        }
                        break;

                    case "3":
                        merk = "Gucci";
                        harga = 200000;
                        break;

                    default:
                        Console.WriteLine("Input Invalid");
                        break;
                }
                Console.WriteLine($"Merk Baju : {merk}");
                Console.WriteLine($"Harga     : {harga}");
                ulangi();
            }
        }
        static void soalSebelas()
        {
            while (true)
            {
                Console.WriteLine("---Soal 11---");
                Console.WriteLine("Belanja Lebaran");

                Console.Write("Masukkan uang Andi : ");
                int uang = int.Parse(Console.ReadLine());
                Console.WriteLine("Masukkan List Harga Baju (pisah menggunakan koma) : ");
                int[] hargaBaju = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);
                Console.Write("Masukkan List Harga Celana (pisah menggunakan koma) : ");
                int[] hargaCelana = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

                int i, j, totalMax = 0;
                for (i = 0; i < hargaBaju.Length; i++)
                {
                    for (j = 0; j < hargaCelana.Length; j++)
                    {
                        int total = hargaBaju[i] + hargaCelana[j];
                        if (total <= uang && total >= totalMax)
                        {
                            totalMax = total;
                        }
                    }
                }
                Console.WriteLine($"Total Harga Maksimum : {totalMax}");
                ulangi();
            }

        }
        static void soalDuaBelas()
        {
            while (true)
            {
                Console.WriteLine("---Soal 12---");
                Console.WriteLine("2D Array");

                Console.Write(" n = ");
                int n = int.Parse(Console.ReadLine());
                int baris, kolom;

                if (n < 0)
                {
                    Console.WriteLine("Input Invalid");
                }
                else
                {
                    for (baris = 0; baris < n; baris++)
                    {
                        for (kolom = 0; kolom < n; kolom++)
                        {
                            if (baris == 0)
                            {
                                Console.Write((kolom + 1) + "\t");
                            }
                            else if (baris == n - 1)
                            {
                                Console.Write(baris - kolom + 1 + "\t");
                            }
                            else if (kolom == 0 || kolom == n - 1)
                            {
                                Console.Write("*\t");
                            }
                            else
                            {
                                Console.Write("\t");
                            }
                        }
                        Console.WriteLine(" ");
                    }
                }
                ulangi();
            }
        }
        static void soalDuaBelas2()
        {
            Console.WriteLine("---Soal 12---");
            Console.WriteLine("2D Array");

            Console.Write(" n = ");
            int n = int.Parse(Console.ReadLine());
            int baris, kolom;

            if (n < 0)
            {
                Console.WriteLine("Input Invalid");
            }
            else
            {
                for (baris = 1; baris <= n; baris++)
                {
                    for (kolom = 1; kolom <= n; kolom++)
                    {
                        if (baris == 1 || baris == n)
                        {
                            if (baris == n)
                            {
                                Console.Write(n - kolom + 1 + "\t");
                            }
                            else
                            {
                                Console.Write(kolom + "\t");
                            }
                        }
                        else 
                        {
                            if (kolom == 1 || kolom == n)
                                Console.Write("*\t");
                            else
                                Console.Write(" \t");
                        }
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
            



        